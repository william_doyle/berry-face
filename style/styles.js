import { StyleSheet } from 'react-native';

/*	William Doyle
 *	Febuary 2021
 *	Details the styling of this app
 *
 * */

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	textInput: {
		borderColor: '#ccc',
		borderWidth: 1,
		marginBottom: 15,
		paddingVertical: 4,
		paddingHorizontal: 2,
		textAlignVertical: 'top',
		width: '95%',
	},
	feedDesc: {
		fontSize: 20,
		margin: 20,
	},
	textTypeOne:{
		fontSize: 15,
	},
	textTypeTwo:{
		fontSize: 20,
	},
	controlBtn: {
		backgroundColor: '#f3f',
		borderWidth: 1,
		borderRadius: 20,
		margin:5,
		padding:5,
	},
	controlDeck: {
		flexDirection: 'row', 
		margin: 3,
		borderWidth: 1,
		borderRadius: 5,
	},
	horizontalBtn: {
		flexDirection: 'row', 
		margin: 3,
		borderRadius: 5,
	},
	shareBtn: {
		padding: 10,
		margin: 10,
		borderColor: '#000',
		borderWidth: 1,
		borderRadius: 5,
	},
	normBtn: {
		backgroundColor: '#95F',

		padding: 10,
		margin: 1,
		borderColor: '#fff',
		borderWidth: 1,
		borderRadius: 5,
		alignItems: 'center',

	},
	normBtnDISABLED: {
		backgroundColor: '#ccc',

		padding: 10,
		margin: 1,
		borderColor: '#fff',
		borderWidth: 1,
		borderRadius: 5,
		alignItems: 'center',
	},
	disabledText:{
		color: '#eee',
	},
	hBtnPair: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',

	},
	stdText:{
		color: '#000',
	},
	episodeName: {
		fontSize: 18,
		flex: 1,
		flexWrap: 'wrap',
	}
});
