import * as React from 'react';
import { Alert, } from 'react-native';
import * as FileSystem from 'expo-file-system';
import * as SQLite from 'expo-sqlite';

/*	Download.js
 *	William Doyle
 *	Download A Podcast Episode + Relevent Data then save that information to an sqlite database
 *	April 14th 2021
 * */

function fileExtension (file) {
	return file.split('.').pop();
}

const localDB = SQLite.openDatabase('podcastDB');

export function ListDownloads(setHook) {
	// Find all episodes from db. Use setHook (a passed in hook setter) to push episode data into state 
	localDB.transaction(
		tx => {
			tx.executeSql('SELECT * from podcastdownloads',
				[],
				(_, { rows }) => {
					console.log('GOT ROWS');

					setHook([]);

					let entries = rows._array;
					let consolidatedArr = [];

					entries.forEach((entry) => {
						consolidatedArr = [...consolidatedArr, {id: entry.id, localUri: entry.localUri, feedId: entry.feedId, title: entry.title  }];
					});
					setHook(consolidatedArr);
				},
				(_, result) => {
					console.log('SELECT failed!');
					console.log(result);
				}
			);
		});
}

export async function Download (episode) {
	console.log('Download started');
	//console.log(`Inside Download(). Episode is -> ${JSON.stringify(episode)}`);
	// 1. Download audio from 'audioUrl'
	let saveUri = `${FileSystem.documentDirectory}${episode.title}${fileExtension(episode.audioUrl)}`;
	// Make sure filename is unique
	for(let i = 0; (await FileSystem.getInfoAsync(saveUri)).exists; i++) {
		saveUri = `${FileSystem.documentDirectory}${episode.title}(${i})${fileExtension(episode.audioUrl)}`;
	}
	try {
		await FileSystem.downloadAsync(episode.audioUrl, saveUri);
	}
	catch (err){
		console.log(`DOWNLOAD FAILED! Error: ${err} `);
		return;	// dont want to save bad data to sql db
	}

	Alert.alert('Download Success!', saveUri);

	// 2. Save download position to sqlite database

	// 2. A) make dable if it doesn't excist
	localDB.transaction( tx => { 
		tx.executeSql(`create table if not exists podcastdownloads (
				id			integer primary key not null,
		localUri	text,
		feedId		text,
		title		text);`,
			[],
			() => console.log('TABLE CREATED OR EXCISTS'), 
			(_, rslt) => console.log(`Could not create table: ${rslt}`)
		);
	});

	// 2. B) Add Data to table
	localDB.transaction(tx => { 
		tx.executeSql('insert into podcastdownloads (localUri, feedId, title) values (?,?,?)',
			[saveUri, episode.feedID, episode.title],
			(_, { rowsAffected }) => console.log(`${rowsAffected} ROW(S) inserted`),
			(_, rslt) => console.log(`could not save data to db --> ${rslt}`)
		);
	});
}
