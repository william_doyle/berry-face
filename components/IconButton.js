import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import styles from '../style/styles.js';
import { Icon } from 'react-native-elements';

/*	IconButton
 *	March 26th 2021
 *	William Doyle
 *	A button with an icon
 *
 *	PROPS
 *		props.disabled	->	[boolean]	is this button disabled?
 *		props.action	->	[function]	thing to do when pressed
 *		props.iconName	->	[string]	the name of the icon you want 
 *		props.type		->	[string]	the icon set you wish to use
 * */
export default function IconButton (props) {
	return (
		<TouchableOpacity 
			disabled={props.disabled}
			style={props.disabled? styles.normBtnDISABLED:styles.normBtn}
			onPress={props.action}
		>
			<Icon 
				name={props.iconName}
				type={props.type /*seems that props.type being undefined is not a problem*/}
			/>
		</TouchableOpacity>
	);
}


