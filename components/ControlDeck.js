import * as React from 'react';
import {useSelector, useDispatch } from 'react-redux';
import ACTIONS from '../redux/actions.js';
import NormalButton from './NormalButton.js';
import IconButton from './IconButton.js';
import styles from '../style/styles.js';
import { View } from 'react-native';

/*	William Doyle
 *	ControlDeck
 *	User interface for play/pause and time jumping
 *	March 26th 2021
 * */
export default function ControlDeck () {
	const appState = useSelector(state => state);
	const dispatch = useDispatch();

	async function JumpBy(milsec){
		let status = await appState.soundObj.getStatusAsync();
		let pos = status.positionMillis;
		await appState.soundObj.pauseAsync();
		await appState.soundObj.playFromPositionAsync(pos + milsec);
		dispatch(ACTIONS.setIsPaused(false));
	}

	return(
		<View style={styles.controlDeck}>
			<NormalButton title="-10m" action={()=>JumpBy(-1000*60*10)} 
				disabled={appState.nowPlaying === null}
			/>
			<NormalButton title="-5m" action={()=>JumpBy(-500*60*10)} disabled={appState.nowPlaying === null}/>
			<NormalButton title="-10s" action={()=>JumpBy(-10000)} disabled={appState.nowPlaying === null}/>

			<IconButton
				type="font-awesome"
				iconName={appState.isPaused? 'play':'pause' }  
				action={
					async () => {
						let status = await appState.soundObj.getStatusAsync();
						if (('isPlaying' in status) && (status.isPlaying)){
							await appState.soundObj.pauseAsync();
							dispatch(ACTIONS.setIsPaused(true));
						}
						else {
							await appState.soundObj.playAsync();
							dispatch(ACTIONS.setIsPaused(false));
						}
					}
				} 
				disabled={appState.nowPlaying === null}/>

			<NormalButton title="+30s" action={()=>JumpBy(30000)} disabled={appState.nowPlaying === null}/>
			<NormalButton title="+5m" action={()=>JumpBy(500*60*10)} disabled={appState.nowPlaying === null}/>
			<NormalButton title="+10m" action={()=>JumpBy(1000*60*10)} disabled={appState.nowPlaying === null}/>
		</View>
	);
}

