import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ACTIONS from '../redux/actions.js';
import IconButton from './IconButton.js';
import { Audio } from 'expo-av';

/*	PlayButton
 *	April 15th 2021
 *	A single consolidated button component for playing audio (this is not play/pause. this is only playing for the first time)
 *	William Doyle
 * */
export default function PlayButton (props) {
	const appState = useSelector(state => state);
	const dispatch = useDispatch();

	async function DownloadAudio(url) {
		await Audio.setAudioModeAsync({
			shouldDuckAndroid: true,
			interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
			playThroughEarpieceAndroid: false,
			staysActiveInBackground: true,
		});

		if (appState.soundObj === null)
			dispatch(ACTIONS.setSound(new Audio.Sound()));

		try {
			await appState.soundObj.loadAsync({uri: url});
			//console.info('loaded');
			await appState.soundObj.setStatusAsync({isLooping: false});
			await appState.soundObj.playAsync();
		}
		catch(err){
			console.log(`Error Downloading Audio -> ${err}`);
		}
	}

	/*	Use when Audio file is already downloaded
	 * */
	async function LoadLocalAudio(url) {
		await Audio.setAudioModeAsync({
			shouldDuckAndroid: true,
			interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
			playThroughEarpieceAndroid: false,
			staysActiveInBackground: true,
		});

		if (appState.soundObj === null)
			dispatch(ACTIONS.setSound(new Audio.Sound()));

		try {
			await appState.soundObj.loadAsync({uri: url});
			//console.info('loaded');
			await appState.soundObj.setStatusAsync({isLooping: false});
			await appState.soundObj.playAsync();
		}
		catch(err){
			console.log(`Error loading Audio -> ${err}`);
		}
	}

	return (
		<IconButton iconName="play" type="ionicon" action={async () => {
			// unload if already playing
			if (appState.soundObj !== null) {
				if (appState.nowPlaying ){
					let status = await appState.soundObj.getStatusAsync();
					dispatch(ACTIONS.setPlayStack([...appState.playStack, {episode: appState.nowPlaying, position: status.positionMillis, length: status.durationMillis}] ));
				}

				let status = await appState.soundObj.getStatusAsync();
				if (status.isLoaded){
					try {
						//console.info('unloading sound obj');
						await appState.soundObj.stopAsync();
						await appState.soundObj.unloadAsync();
					}
					catch (err){
						console.error(`An error occured while trying to stop and unload audio: ${err}`);
					}
				}
				else 
					console.info('Audio already unloaded');
			}

			// push whatever is currently playing to the play stack
			dispatch(ACTIONS.setNowPlaying(props.ep));
			if (props.local)
				LoadLocalAudio(props.ep.localUri);
			else 
				DownloadAudio(props.ep.audioUrl);
		}}	/>
	);
}
