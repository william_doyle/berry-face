import * as React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import styles from '../style/styles.js';

/*	NormalButton
 *	March 26th 2021
 *	William Doyle
 *	A normal button with text
 *
 *	PROPS
 *		props.disabled	->	[boolean]	is this button disabled?
 *		props.title		->	[string]	title of button
 *		props.action	->	[function]	thing to do when pressed
 * */
export default function NormalButton (props) {
	return (
		<TouchableOpacity 
			disabled={props.disabled}
			style={props.disabled? styles.normBtnDISABLED:styles.normBtn}
			onPress={props.action}
		>
			<Text style={props.disabled? styles.disabledText:styles.stdText }>
				{props.title}
			</Text>
		</TouchableOpacity>
	);
}
