import * as React from 'react';
import { ActivityIndicator, View, Alert, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import PlayButton from './PlayButton.js';
import IconButton from './IconButton.js';
import { Download } from '../Download.js';
import styles from '../style/styles.js';
import { useSelector, useDispatch } from 'react-redux';

export default function EpisodePreview(props) {

	const appState = useSelector(state => state);
	return (
		<View key={props.index} style={{flexDirection: 'row', paddingTop: 4}}>
			{
				(appState.settings.showEpisodeArt === true) &&
					<Image source={{uri: props.ep.artworkUrl}} style={{width: 50, height: 50}} />
			}
			<IconButton 
				iconName="download-outline" 
				type="ionicon" 
				disabled={props.downloaded}
				action={() => Download(props.ep)}/>
			<IconButton 
				iconName="information-circle-outline" 
				type="ionicon" 
				action={() => {
					console.log(`Artwork URL ->\t${props.ep.artworkUrl}`);
					Alert.alert('Episode Data',JSON.stringify(props.ep, 4));
				}}/>

			<PlayButton ep={props.ep} local={false}/>
			<Text style={styles.episodeName}>{props.ep.title}</Text>
		</View>
	);
}
