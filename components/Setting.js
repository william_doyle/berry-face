import * as React from 'react';
import { ActivityIndicator, View, Alert, Text, TextInput, Image, ScrollView, TouchableOpacity, Switch } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import styles from '../style/styles.js';

import NormalButton from './NormalButton.js';
import ACTIONS from '../redux/actions.js';

// DO NOT EXPORT
function NormalSetting(props){
	const appState = useSelector(state => state);
	const dispatch = useDispatch();

	const [reg, setReg] = React.useState('');	// reg as in "registier", as in sometimes a variable is best thought of as what it is, a useful piece of memory 

	React.useEffect(() => {
		setReg(appState.settings[props.responsibility]);
	}, []);

	return (
		<View>
			<Text>{props.title}</Text>
			{
				(props.control === undefined) &&
					<View>
						<TextInput
							value={`${reg}`}
							onChangeText={text => {
								setReg(text);
							}}
							style={styles.textInput}
							keyboardType={props.keyboardType}
						/>
						<NormalButton 
							title="Save" 
							action={() => dispatch(ACTIONS.mod_settings({...appState.settings, [props.responsibility]: reg}))}/>
					</View>

			}
			{
				(props.control !== undefined) && props.control
			}
		</View>
	);
}


/*	May 2nd 2021
 *	William Doyle
 *	NumericalSetting
 * */
export function NumericalSetting(props){
	return (<NormalSetting {...props} keyboardType="numeric"/>);
}

/*	May 2nd 2021
 *	William Doyle
 *	BooleanSetting
 * */
export function BooleanSetting(props){
	const appState = useSelector(state => state);
	const dispatch = useDispatch();
	return (<NormalSetting {...props} 
		keyboardType="default" 
		control={<Switch 
			value={appState.settings[props.responsibility]} 
			onValueChange={newVal => dispatch(ACTIONS.mod_settings({...appState.settings, [props.responsibility]: newVal}))}/>
		} />);
}




