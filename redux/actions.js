// define your action types
const Types = {
	ADD_SUBSCRIPTION: 'ADD_SUBSCRIPTION',
	RESET_SUBSCRIPTION: 'RESET_SUBSCRIPTION'
};

// this is the function that you will call dispatch() on
// when you dispatch the action, the object retruned will 
// be intercepted by the reducer we set up 
function addSubscription(subscription) { 
	// this is the data used in the reducer
	return { type: Types.ADD_SUBSCRIPTION, subscription};
}

function resetSubscription() {
	return { type: Types.RESET_SUBSCRIPTION};
}

function setPodData (poddata){
	return {
		type: 'SETPODDATA',
		poddata
	};
}

function setSubscriptions (subs){
	return {
		type: 'SETSUBS',
		subs
	};
}

function setIsLoggedIn(isLoggedIn){
	return {
		type: 'SETISLOGGEDIN',
		isLoggedIn
	};
}

function setSound(soundObj) {
	return {
		type: 'SETSOUND',
		soundObj,
	};
}

function setNowPlaying (episode) {
	return {
		type: 'SETNOWPLAYING',
		episode,
	};
}

function setIsPaused (isPaused) {
	return {
		type: 'SETISPAUSED',
		isPaused,
	};
}

function setPlayStack(playStack) {
	return {
		type: 'SETPLAYSTACK',
		playStack,
	};
}

function mod_settings(settingsObj){

	return {
		type: 'MOD_SETTINGS',
		settingsObj,
	};

}

export default {
	addSubscription,
	resetSubscription,
	setPodData,
	setIsLoggedIn,
	setSubscriptions,
	setSound,
	setNowPlaying,
	setIsPaused,
	setPlayStack,
	mod_settings,
	Types
};
