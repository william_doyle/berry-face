import ACTIONS from './actions';

const defaultSettings = {
	searchResultCount: 5,
	showEpisodeArt: false,
};

const initialState = {
	subscriptions: [],
	poddata: null,
	userSubsDbRslt: [],
	isLoggedIn: false,
	soundObj: null,

	nowPlaying: null,
	isPaused: false,
	playStack: [],

	settings: defaultSettings,
};

function subscriptionsDataReducer(state = initialState, action){
	// when an action is dispatched it will first go to actions.js, 
	// from there it will then be intercepted by this reducer
	// based on the action that was dispatched it will then be 
	// picked up by one of the cases in this switch statement 
	// and the state will change based on the logic inside the case statement
	switch(action.type) {
		case ACTIONS.Types.ADD_SUBSCRIPTION:
			{
				return {
					...state,
					subscriptions: [...state.subscriptions, action.subscription]
				}
			}
		case ACTIONS.Types.RESET_SUBSCRIPTION:
			{
				console.log('reducers.js has a problem. RESET_SUBSCRIPTION seems wrong. If you see this msg please come fix. ');
				return Object.assign({}, state, {
					subscriptions: initialState // this is wrong
				})
			}
		case 'SETPODDATA':
			{

			}

		case 'SETSUBS':
			{
				return{ 
					...state, 
					subscriptions: action.subs
				}
			}
		case 'SETISLOGGEDIN':
			{
				return {
					...state,
					isLoggedIn: action.isLoggedIn

				}
			}
		case 'SETSOUND':
			{
				return {
					...state,
					soundObj: action.soundObj
				}
			}
		case 'SETNOWPLAYING':
			{
				return {
					...state,
					nowPlaying: action.episode
				}
			}
		case 'SETISPAUSED':
			{
				return {
					...state,
					isPaused: action.isPaused
				}

			}
		case 'SETPLAYSTACK':
			{
				return {
					...state,
					playStack: action.playStack
				}
			}
		case 'MOD_SETTINGS':
			{
				return {
					...state,
					settings: action.settingsObj,
				}
			}

		default: 
			return state;
	}
}

export default subscriptionsDataReducer;
