import { parse } from 'fast-xml-parser';
import consola from 'consola';
import * as Crypto from 'expo-crypto';

import store from './redux/store.js';


var apiKey = process.env.IDX_APIKEY;
var apiSecret = process.env.IDX_APISECRET;

/*	April 18th 2021
 *	William Doyle
 *	GetEpisodesFromFeed()
 * */
export async function GetEpisodesFromFeed (feed) {

	console.log(JSON.stringify(store));

	if (!('url' in feed))
		console.log('WARNING [GetEpisodesFromFeed(feed)]: feed must contain \'url\'');

	try {
		let result = await fetch(feed.url);
		let astext = await result.text();

		let obj = parse(astext, {
			ignoreAttributes : false,
			parseAttributeValue : true,
		});

		let epis = obj.rss.channel.item.map(ep => {
			return {
				title: ep.title,
				audioUrl: ep.enclosure['@_url'],
				imageUrl: feed.image,//obj.rss.channel.image.url,
				artworkUrl: ep['itunes:image']['@_href'],
				description: ep.description,
				feedID: feed.id,
				location: ('podcast:location' in obj.rss.channel)? obj.rss.channel['podcast:location']['@_geo']:'unknown',
				value:  ('podcast:value' in obj.rss.channel)? obj.rss.channel['podcast:value']:'unknown',
			};
		});
		//	setEpisodes(epis);
		return epis;
	}
	catch(err) {
		consola.error(`Error! Could not fetch xml --> ${err}`);
	}
}

/*	April 18th 2021
 *	William Doyle
 *	SearchPodcastIndexWithHook()
 *	@depricated
 *	use SearchPodcastIndex() instead
 * */
export function SearchPodcastIndexWithHook (term, setHook) {
	
	const state = store.getState();

	var apiHeaderTime = Math.floor(Date.now()/1000); 
	let options;
	var myurl = `https://api.podcastindex.org/api/1.0/search/byterm?q=${term}&max=${state.settings.searchResultCount}`; 

	Crypto.digestStringAsync( Crypto.CryptoDigestAlgorithm.SHA1, `${apiKey + apiSecret + apiHeaderTime}` )
		.then((result) => {
			options = 
				{  method: 'get',
					headers: { 
						'X-Auth-Date': ''+apiHeaderTime,
						'X-Auth-Key': apiKey,
						'Authorization': result,
						'User-Agent': 'BerryFace/1.8'
					},
				};
			return options;
		})
		.then((options) => {
			fetch(myurl, options)
				.then(res => res.json())
				.then((json) =>	setHook(json))
				.catch((err) => console.log(`err (prob caused by fetch) -> ${err}`));
		})
		.catch((error) => console.log(`fetch must have caused an error here it is -> ${error}`));
}

/*	April 18th 2021
 *	William Doyle
 *	SearchPodcastIndex()
 * */
export async function SearchPodcastIndex (term) {

	const state = store.getState();
	var apiHeaderTime = Math.floor(Date.now()/1000); 
	var myurl = `https://api.podcastindex.org/api/1.0/search/byterm?q=${term}&max=${state.settings.searchResultCount}`; 

	try{
		let options = {  method: 'get',
			headers: { 
				'X-Auth-Date': ''+apiHeaderTime,
				'X-Auth-Key': apiKey,
				'Authorization': await Crypto.digestStringAsync( Crypto.CryptoDigestAlgorithm.SHA1, `${apiKey + apiSecret + apiHeaderTime}` ),
				'User-Agent': 'BerryFace/1.8'
			},
		};

		let result = await fetch(myurl, options);
		return result.json();
	}
	catch(err){
		console.log(`err (prob caused by fetch or Crypto) -> ${err}`);
	}
}

/*	William Doyle
 *	Febuary 9th and April 18th 2021
 *	GetPodcastDataByFeedId()
 * */
export async function GetPodcastDataByFeedId(feedid) {
	var apiHeaderTime = Math.floor(Date.now()/1000);
	var url = `https://api.podcastindex.org/api/1.0/podcasts/byfeedid?id=${feedid}`; 

	try{
		var hash4Header = await Crypto.digestStringAsync( Crypto.CryptoDigestAlgorithm.SHA1, `${apiKey + apiSecret + apiHeaderTime}` ); 
		let options = 
			{  method: 'get',
				headers: { 
					// not needed right now, maybe in future:  "Content-Type": "application/json",
					'X-Auth-Date': ''+apiHeaderTime,
					'X-Auth-Key': apiKey,
					'Authorization': hash4Header,
					'User-Agent': 'SuperPodcastPlayer/1.8'
				},
			};
		return fetch(url, options)
			.then(res => res.json())
			.catch(err => console.log(`bad thing happened --> ${err}`));
	}catch (err){
		console.log(`error! ${err}`);
	}
}

