import * as React from 'react';
import { useSelector } from 'react-redux';
import { View, Image, ScrollView, Alert } from 'react-native';
import NormalButton from '../components/NormalButton.js';
import IconButton from '../components/IconButton.js';
import ControlDeck from '../components/ControlDeck.js';
import styles from '../style/styles.js';
import * as firebase from 'firebase';
import '@firebase/firestore';

/*	MainScreen()
 *	William Doyle
 *	Febuary 2021
 *
 *	The main screen of our app. Should be displayed once the user is signed in. 
 * */
export default function MainScreen ({navigation, route}) {

	const appState = useSelector(state => state);

	/*	TopMembers()
	 *	Stuff that goes in the top bar of the screen
	 * */
	function TopMemebers() {
		return (
			<Image 
				source={{uri: `${firebase.auth().currentUser.photoURL}`}} 
				style={{width: 50, height: 50, borderRadius: 100, marginRight: 20}}/>
		);
	}

	React.useEffect(() => {
		navigation.setOptions({headerRight: TopMemebers});
	}, []);

	
	return (
		<View style={styles.container}>
			<ControlDeck/>
			<ScrollView>
				<NormalButton 
					title="Now Playing" 
					action={() => navigation.navigate('NowPlaying', {})} 
					disabled={appState.nowPlaying === null}/>

				<View style={styles.hBtnPair}>
					<NormalButton 
						title="Your Subscriptions" 
						action={() => navigation.navigate('Subscriptions', {})} 
						disabled={false}/>
					<IconButton 
						iconName="search" 
						action={() => navigation.navigate('Search', {})} 
						disabled={false}/>
				</View>

				<IconButton 
					iconName="inventory" 
					action={() => navigation.navigate('ViewDownloads', {})} 
					disabled={false}/>
				<IconButton 
					iconName="settings" 
					action={() => navigation.navigate('SettingsScreen', {})} 
					disabled={false}/>
				<IconButton 
					iconName="database" 
					type="octicon" 
					action={() => {
//						navigation.navigate('PlayStackScreen', {}); // uncomment to enable play stack screen
						Alert.alert('Broken', 'Sorry, this feature is not part of the \'School Project\' build. It\'s currently unstable and causes errors.');
					}} 
					disabled={!appState.playStack.length > 0}/>
			</ScrollView>
		</View>
	);
}
