import * as React from 'react';
import {useSelector, useDispatch } from 'react-redux';
import ACTIONS from '../redux/actions.js';
import {ActivityIndicator, View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../style/styles.js';
import { GetUsersSubscriptions, GetUsersSubscriptionsRealTime } from '../UserInfo.js';
import { Dimensions } from 'react-native';
import ControlDeck from '../components/ControlDeck.js';


import * as firebase from 'firebase';
import '@firebase/firestore';

export default function ViewSubscriptions ({navigation, route}) {
	const appState = useSelector(state => state);
	const dispatch = useDispatch();

	React.useEffect(() => {
		if ((typeof(appState.subscriptions) == 'undefined') || (!(appState.subscriptions.length > 0))){
			console.log('subs undefined. Try and load them once');
			//LoadSubsRealTime();
			GetUsersSubscriptionsRealTime();
		}
	}, []);

	return (
		<View style={styles.container}>
			<ControlDeck/>
			<ScrollView>
				{
					((typeof(appState.subscriptions) == 'undefined') || (!(appState.subscriptions.length > 0)))?
						<ActivityIndicator color='#999999'/>
						:
						appState.subscriptions.map((item, index) => 
							//Object.values(realtimeSubs).map((item, index) =>
							<TouchableOpacity 
								key={index}
								onPress={() => navigation.navigate('ViewFeed', {_feed: item.feed })}>
								<Image 
									source={{uri: item.feed.artwork}} 
									style={{width: Dimensions.get('window').width, height: Dimensions.get('window').width}}/>
							</TouchableOpacity>)
				}
			</ScrollView>
		</View>
	);
}
