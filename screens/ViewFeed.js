import * as React from 'react';
import { ActivityIndicator, View, Alert, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../style/styles.js';
import { Audio } from 'expo-av';
import { useSelector, useDispatch } from 'react-redux';
import ACTIONS from '../redux/actions.js';
import { AddSubscription } from '../UserInfo.js';
import { Dimensions } from 'react-native';
import ControlDeck from '../components/ControlDeck.js';
import IconButton from '../components/IconButton.js';
import NormalButton from '../components/NormalButton.js';
import { Download, ListDownloads } from '../Download.js';
import PlayButton from '../components/PlayButton.js';
import EpisodePreview from '../components/EpisodePreview.js';
import { GetEpisodesFromFeed } from '../Podcast.js';

/*	William Doyle
 *	ViewFeed
 *	
 *	Allow the user to get all the data they may want about a podcast feed.
 *	This will include the ability to select an episode and go to the ViewEpisode 
 *	page
 * */
export function ViewFeed ({navigation, route}) {
	const [episodes, setEpisodes] = React.useState([]);

	const [downloaded, setDownloaded] = React.useState([]);

	const appState = useSelector(state => state);
	const dispatch = useDispatch();

	/*	isSubscribed()
	 *	Returns true if user is subscribed to current podcast
	 *	otherwise returns false
	 *	William Doyle
	 *	May 2nd 2021
	 * */
	function isSubscribed() {
		return appState.subscriptions.find(s => s.feed.id === route.params._feed.id) !== undefined;
	}

	async function WaitAndSetEpisodes(feed){
		setEpisodes(await GetEpisodesFromFeed(feed));
	}

	React.useEffect(() => {
		navigation.setOptions({title: route.params._feed.title}); // set header title to feed title
		if (appState.soundObj === null)
			dispatch(ACTIONS.setSound(new Audio.Sound()));
		WaitAndSetEpisodes(route.params._feed);

		// Hook into sql download logic and load list of episodes
		ListDownloads(setDownloaded);
		// now remove episodes that do not belong to this feed
		let tempFilteredDownloads = downloaded.filter(dwep => dwep.feedId == route.params._feed.id);
		setDownloaded(tempFilteredDownloads);
	}, []);

	// visuals
	return (
		<View style={styles.container}>
			<ControlDeck/>
			<ScrollView style={{width: Dimensions.get('window').width}}>
				<Text style={styles.feedDesc}>
					{route.params._feed.description}
				</Text>

				<TouchableOpacity onPress={() => {Alert.alert(`id: ${route.params._feed.id}\nAuthors: ${route.params._feed.author}`)}}>
					<Image source={{uri: route.params._feed.image}} style={{width: Dimensions.get('window').width, height: Dimensions.get('window').width}} />
				</TouchableOpacity>
				<NormalButton 
					title={'Subscribe'} 
					disabled={isSubscribed()} 
					action={() => {
						AddSubscription(route.params._feed.id, route.params._feed.title);
						//Alert.alert('Subscribed!');
					}} />
				<Text>{route.params._feed.url}</Text>
				{
					(episodes.length === 0) && <View>
						<ActivityIndicator color='#999999'/>
					</View>
				}

				{
					episodes.map((ep, index) => <EpisodePreview index={index} ep={ep} downloaded={downloaded.find(dwep => dwep.title == ep.title)}/>)
				}
			</ScrollView>
		</View>
	);
}
