import * as React from 'react';
import { ActivityIndicator, View, Alert, Text, TextInput, Image, ScrollView, TouchableOpacity } from 'react-native';
import styles from '../style/styles.js';

import { NumericalSetting, BooleanSetting } from '../components/Setting.js';

export default function SettingsScreen ({navigation, route}) {

	return (
		<ScrollView>
			<NumericalSetting title="Search Result Limit" responsibility="searchResultCount" />
			<BooleanSetting title="Show Episode Art" responsibility="showEpisodeArt" />

		</ScrollView>
	);
}
