import * as React from 'react';
import { View, Alert, Text, Image, TouchableOpacity } from 'react-native';
import {useSelector, useDispatch } from 'react-redux';
import styles from '../style/styles.js';
import * as Progress from 'react-native-progress';
import { Dimensions } from 'react-native';
import * as MailComposer from 'expo-mail-composer';
import IconButton from '../components/IconButton.js';
import ControlDeck from '../components/ControlDeck.js';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
/*
 *
 * */
export default function NowPlayingScreen ({navigation, route}) {
	const [ratio, setRatio] = React.useState(0);
	const [time, setTime] = React.useState(0);
	const [location, setLocation] = React.useState(null);

	const appState = useSelector(state => state);

	function HeaderButtons () {
		return (
			<View style={styles.horizontalBtn}>
				<IconButton iconName="mail" type="ionicon" action={() => DraftNewEmail()}/>
				<IconButton 
					iconName="flash" 
					type="ionicon" 
					action={() => Alert.alert('Value For Value',`${JSON.stringify(appState.nowPlaying.value)}`)}/>
			</View>
		);
	}

	React.useEffect(() => {
		navigation.setOptions({headerRight: HeaderButtons});

		async function GetLocation() {
			//		let { status } = await Location.requestForegroundPermissionsAsync(); 	// does not work
			let { status } = await Permissions.askAsync(Permissions.LOCATION);		// works (but deprecated)
			if (status !== 'granted') {
				console.log('get location failed');
				return;
			}
			let llocation = await Location.getCurrentPositionAsync({});
			setLocation(llocation);
		}
		GetLocation();

		//console.log(JSON.stringify(appState.nowPlaying));
		const interval = setInterval(() => UpdateRatio(), 1000);
		return () => {
			clearInterval(interval);
		};
	}, []);

	async function DraftNewEmail () {
		let options = {
			recipients: [],
			ccRecipients: [],
			bccRecipients: [],
			subject: 'Check out this podcast',
			body: `${appState.nowPlaying.title}\n\n${appState.nowPlaying.audioUrl}\n\ntime stamp\n ${Math.floor(time/60)} : ${Math.floor(time%60)}`,
			isHtml: false,
			attachments: [],
		};
		try {
			await MailComposer.composeAsync(options);
		} catch (err) {
			Alert.alert(`Error email probably failed ${JSON.stringify(err)}`);
		}
	}

	async function UpdateRatio () {
		let status = await appState.soundObj.getStatusAsync();
		let pos = status.positionMillis;
		let dur = status.durationMillis;
		setTime(pos/1000);
		setRatio(pos/dur);
	}

	return (
		<View style={styles.container}>
			<View style={styles.container}>
				<Text style={styles.feedDesc}>
					{appState.nowPlaying.title}
				</Text>
				{
					(appState.nowPlaying.location !== 'unknown') &&
						<Text>
							{`@ ${appState.nowPlaying.location}`}
						</Text>
				}
				{
					(location !== null) && ('coords' in location) &&
						<Text>
							{`You're @ ${location['coords']['latitude']},${location['coords']['longitude']}`}
						</Text>
				}
				<TouchableOpacity onPress={() => Alert.alert('All Data', JSON.stringify(Object.keys(appState.nowPlaying)))}>
					<Image source={{uri: appState.nowPlaying.imageUrl}} style={{width: Dimensions.get('window').width, height: Dimensions.get('window').width}}/>
				</TouchableOpacity>

				<ControlDeck/>

				<Progress.Bar progress={ratio} width={Dimensions.get('window').width-20} />

				<Text style={styles.feedDesc}>
					{`${Math.floor(time/60)} : ${Math.floor(time%60)}`}
				</Text>
			</View>
		</View>
	);
}
