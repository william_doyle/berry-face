import * as React from 'react';
import { View, Alert, Text, ScrollView, } from 'react-native';
import styles from '../style/styles.js';
import IconButton from '../components/IconButton.js';
import PlayButton from '../components/PlayButton.js';
import { ListDownloads } from '../Download.js';
import ControlDeck from '../components/ControlDeck.js';

export default function ViewDownloads ({navigation, route}) {
	const [localEps, setLocalEps] = React.useState([]);

	React.useEffect(() => {
		// load db content into localEps
		ListDownloads(setLocalEps);
	}, []);

	return (
		<View>
			<View style={{	alignItems: 'center'}}>
				<ControlDeck/>
			</View>
			<ScrollView >
				{
					(localEps !== undefined) && localEps.map((ep, index) => 
						<View key={index} style={{flexDirection: 'row', paddingTop: 4}}>
							<PlayButton ep={ep} local={true}/>
							<IconButton iconName="trash" type="ionicon" action={() => Alert.alert('Not Yet Implemented', 'deleting episodes is not yet implemented')} />
							<Text style={styles.episodeName}>{ep.title}</Text>
						</View>
					)
				}
			</ScrollView>
		</View>
	);
}
