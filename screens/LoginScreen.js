import * as React from 'react';
import * as Google from 'expo-auth-session/providers/google';
import { View, Alert, Text, TextInput, Modal } from 'react-native';
import * as firebase from 'firebase';
import styles from '../style/styles.js';
// for redux
import {useDispatch } from 'react-redux';
import ACTIONS from '../redux/actions.js';
import NormalButton from '../components/NormalButton.js';

/*	LoginScreen()
 *	~Febuary 8th 2021
 *	William Doyle
 *
 *	Allow user to login with various options
 *	> Google Sign in
 *	> Email And Password
 *	*/
export default function LoginScreen({navigation}){
	const [authMeth, setAuthMeth] = React.useState('google');
	const [registerMode, setRegisterMode] = React.useState(false);
	const [registrationEmail, setRegistrationEmail] = React.useState('');
	const [registrationPassword, setRegistrationPassword] = React.useState('');
	const [loginEmail, setLoginEmail] = React.useState('');
	const [loginPassword, setLoginPassword] = React.useState('');

	const dispatch = useDispatch();

	const [request, response, promptAsync] = Google.useIdTokenAuthRequest(
		{clientId: '649505560786-il4vnqe8udm4fu78e10vvpd394g76jc3.apps.googleusercontent.com',},
	);

	React.useEffect(() => {

		/*	On Login and logout
		 * */
		firebase.auth().onAuthStateChanged((user) => {
			if (user) {
				console.log('user logged in. going to main screen');
				//loggedIn = true;
				dispatch(ACTIONS.setIsLoggedIn(true));
				navigation.navigate('Main', {});
			}
			else {
				console.log('user logged out. going to login screen');
				//loggedIn = false;
				dispatch(ACTIONS.setIsLoggedIn(false));
				navigation.navigate('Login', {});
			}
		});

		if (response?.type === 'success') {
			const { id_token } = response.params;

			const credential = firebase.auth.GoogleAuthProvider.credential(id_token);
			firebase.auth().signInWithCredential(credential)
				.then((usr)=> console.log(`User Signed in -> ${usr}`))
				.then(() =>  navigation.navigate('Main', {}))
				.catch(error => console.log(`JPL please be advised an error has occured: ${error}`) );
		}
	}, [response]);

	function registerWithFirebase  ()  {
		if (registrationEmail.length < 4) {
			Alert.alert('Please enter an email address.');
			return;
		}

		if (registrationPassword.length < 4) {
			Alert.alert('Please enter a password.');
			return;
		}

		firebase.auth().createUserWithEmailAndPassword(registrationEmail, registrationPassword)
			.then(function (_firebaseUser) {
				Alert.alert('User registered!');

				setRegistrationEmail('');
				setRegistrationPassword('');
			})
			.catch(function (error) {
				var errorCode = error.code;
				var errorMessage = error.message;

				if (errorCode == 'auth/weak-password') 
					Alert.alert('The password is too weak.');
				else 
					Alert.alert(errorMessage);
				//console.log(error);
			});
	}

	/*	Log the user in
	 * */
	function loginWithFirebase  ()  {
		if (loginEmail.length < 4) {
			Alert.alert('Please enter an email address.');
			return;
		}

		if (loginPassword.length < 4) {
			Alert.alert('Please enter a password.');
			return;
		}

		firebase.auth().signInWithEmailAndPassword(loginEmail, loginPassword)
			.then(function (_firebaseUser) {
				navigation.navigate('Main', {/*signout_func: signoutWithFirebase*/} )
			})
			.catch(function (error) {
				var errorCode = error.code;
				var errorMessage = error.message;

				if (errorCode === 'auth/wrong-password') 
					Alert.alert('Wrong password.');
				else 
					Alert.alert(errorMessage);
			});
	}

	return (
		<View style={styles.container}>
			{
				(authMeth === 'google') &&
					<View style={styles.container}>
						<NormalButton title={'Login With Google'} action={() => promptAsync()} disabled={!request}/>
						<NormalButton title={'Logout'} action={() => Alert.alert('Not Implemented','Loging out is not yet implemented')} disabled={!request}/>
						<NormalButton title='Oops!' action={() => navigation.navigate('Main', {})} disabled={false} />
					</View>
			}{
				(authMeth === 'email') && 
					<View style={styles.container}>
						<View>
							<Modal
								animationType="slide"
								transparent={false}
								visible={registerMode}
								onRequestClose={() => {	}}> 
								<View style={styles.container}>
									<Text style={styles.label}>Register with Firebase</Text>
									<TextInput
										style={styles.textInput}
										onChangeText={ (value) => setRegistrationEmail(value) }
										autoCapitalize="none"
										autoCorrect={false}
										autoCompleteType="email"
										keyboardType="email-address"
										placeholder="email"
									/>
									<TextInput
										style={styles.textInput}
										onChangeText={ (value) => setRegistrationPassword(value) }
										autoCapitalize="none"
										autoCorrect={false}
										autoCompleteType="password"
										keyboardType="visible-password"
										placeholder="password"
									/>
									<NormalButton 
										title="Register" 
										action={() => {
											registerWithFirebase();
											setRegisterMode(!registerMode);

										}} />
									<NormalButton 
										title={registerMode ? 'Already Registered?':'Not Yet Registered?'}
										action={() => {
											setRegisterMode(!registerMode);
										}}/>
								</View>
							</Modal>
							<View>
								<Text style={styles.label}>Sign In with Firebase</Text>
								<TextInput
									style={styles.textInput}
									onChangeText={ (value) => setLoginEmail(value) }
									autoCapitalize="none"
									autoCorrect={false}
									autoCompleteType="email"
									keyboardType="email-address"
									placeholder="email"
								/>
								<TextInput
									style={styles.textInput}
									onChangeText={ (value) => setLoginPassword(value) }
									autoCapitalize="none"
									autoCorrect={false}
									autoCompleteType="password"
									keyboardType="visible-password"
									placeholder="password"
								/>
								<NormalButton 
									title="Login" 
									action={() => {
										loginWithFirebase();
									}} />
								<NormalButton 
									title={registerMode ? 'Already Registered?':'Not Yet Registered?'}
									action={() => {
										setRegisterMode(!registerMode);
									}}/>
							</View>
						</View>
					</View>
			}
			<NormalButton title={(authMeth === 'google')? 'Or Sign In With Email & Password':'Or sign in with google'} action={() => setAuthMeth((authMeth === 'google')? 'email':'google')} />
		</View>
	);
}
