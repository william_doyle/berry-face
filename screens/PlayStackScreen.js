import * as React from 'react';
import { View, Text, ScrollView, } from 'react-native';
import ControlDeck from '../components/ControlDeck.js';
import styles from '../style/styles.js';
import { useSelector, useDispatch } from 'react-redux';
import ACTIONS from '../redux/actions.js'
import NormalButton from '../components/NormalButton.js';
import consola from 'consola';
import * as Progress from 'react-native-progress';
import * as SQLite from 'expo-sqlite';

function DiscardedPlayable (props) {
	const appState = useSelector(state => state);
	const dispatch = useDispatch();

	async function Resume () {
		if (appState.soundObj !== null) {
			if (appState.nowPlaying ){
				let status = await appState.soundObj.getStatusAsync();
				// Filter the "played stack" (I know its an array) by two pieces of data (incase you're listening to two shows with the same of one of those datiums)
				// this filter is applied to stop the stack from having the same episode with multiple timestamps
				props.sqlAccesser(appState.nowPlaying);
				let filteredArr = appState.playStack.filter(e => (e.episode.description !== appState.nowPlaying.description && e.episode.title !== appState.nowPlaying.title));
	//			dispatch(ACTIONS.setPlayStack([...filteredArr, {episode: appState.nowPlaying, position: status.positionMillis, length: status.durationMillis }]));
			}

			let status = await appState.soundObj.getStatusAsync();
			if (status.isLoaded){
				try {
					//consola.info('unloading sound obj');
					await appState.soundObj.stopAsync();
					await appState.soundObj.unloadAsync();
				}
				catch (err){
					consola.error(`An error occured while trying to stop and unload audio: ${err}`);
				}
			}
			else 
				consola.info('Audio already unloaded');
		}

		dispatch(ACTIONS.setNowPlaying(props.episode));
		await appState.soundObj.loadAsync({uri: props.episode.audioUrl });
		await appState.soundObj.playFromPositionAsync(props.position);
	}

	return (
		<View>
			<NormalButton 
				title='Resume'
				action={ () => Resume() }
				disabled={false}/>
			<Text>{props.episode.title}</Text>
			<Text>{`pos: ${props.position}`}</Text>
		</View>
	);
}

export default function PlayStackScreen({navigation}){

	function ManageSQL (episode) {
		let loadArr = [];

		const db = SQLite.openDatabase('podcastsDB');
		const TABLE_NAME = 'INTERUPTED_SHOWS'; 

		function CreateTable () {
			db.transaction(tx => {
				tx.executeSql(
					`create table if not exists ${TABLE_NAME} (
					id				integer primary key not null,
					position		integer,
					length			integer,
					title			text,
					audioUrl		text,
					imageUrl		text,
					description		text
				);`,
					[],
					() => consola.info('TABLE CREATED!'),
					(_, result) => consola.error(`CREATE TABLE FAILED! details -> ${result}`)
				);
			});
		}

		function InsertEpisode (e) {
			db.transaction(tx => {
				tx.executeSql(
					`insert into ${TABLE_NAME} (position, length, title, audioUrl, imageUrl, description) values (?,?,?,?,?,?)`,
					[e.position, e.length, e.episode.title, e.episode.audioUrl, e.episode.imageUrl, e.episode.description],
					(_, {rowsAffected}) => {
						if (rowsAffected > 0)
							consola.info(`${rowsAffected} ROW(s) inserted`);
						else 
							consola.error('INSERT FAILED');
					},
					(_, result) => consola.error(`INSERT failed cause -> ${result}`)
				);
			});
		}

		function DeleteEpisode (e) {
			db.transaction(tx => {
				tx.executeSql(`delete from ${TABLE_NAME} where title = ${e.episode.title} and description = ${e.episode.description});`,
					[],
					(_, {rowsAffected}) => {
						if (rowsAffected > 0){
							consola.info(`${rowsAffected} ROW(s) deleted`);
							if (rowsAffected > 1)
								consola.warn(`Oops! Did you mean to delete ${rowsAffected} rows?`);
						}
						else 
							consola.error('INSERT FAILED');
					},
					(_, result) => consola.error(`DELETE failed cause -> ${result}`)
				);
			});
		}


		if (episode !== null){
			CreateTable();			// make sure table excists
			DeleteEpisode(episode);	// delete the episode if its already stored in db
			InsertEpisode(episode);	// save the episode to the db
		}
		else {
			// extract sql data and put into redux managed state
			try {
				db.transaction(tx => {
					tx.executeSql(
						`select * from ${TABLE_NAME}`,
						[],
						(_, { rows }) => {
							consola.info('We have data!');
							rows._array.map((edata, index) => loadArr.push({episode: {title: edata.title, audioUrl: edata.audioUrl, imageUrl: edata.imageUrl, description: edata.description}, position: edata.position, length: edata.length }));
						},
						(_, result) => consola.error('select failed')
					);
				});
			}
			catch (err){
				// logial dependence on try/catch ... this is a violation of my own rules
				consola.error('What would you have me do?');
			}
			dispatch(ACTIONS.setPlayStack(loadArr));
		}
	}

	const appState = useSelector(state => state);
	const dispatch = useDispatch();

	return (
		<View style={styles.container}>
			<ControlDeck/>
			<ScrollView>
				{
					appState.playStack.map((playable, index) => (
						<View key={index}>
							<DiscardedPlayable episode={playable.episode} position={playable.position} />
							<Progress.Bar progress={playable.position/(playable.length != 0? playable.length:1)} width={200} />
						</View>
					))
				}
			</ScrollView>
		</View>
	);
}
