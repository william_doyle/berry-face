import * as React from 'react';
import { View, Text, Image, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import styles from '../style/styles.js';
import { Dimensions } from 'react-native';
import ControlDeck from '../components/ControlDeck.js';
import {SearchPodcastIndex} from '../Podcast.js';
const podNoImage = 'http://williamdoyle.ca/img/sum_zero.png'; // should be url of 'broken image'

/*	PodcastSearch()
 *	William Doyle
 *	Febuary 10th 2021
 *
 *	This is where users can search for podcasts
 * */
export default function PodcastSearch ({navigation, route}) {
	const [searchTerm, setSearchTerm] = React.useState('podcasting%202.0');
	let [responseData, setResponseData] = React.useState({});

	React.useEffect(() => {
		let mounted = true;	// flag used to ensure we don't preform state update on unmounted component

		async function PreformSearch () {
			let results = await SearchPodcastIndex(searchTerm);
			if (mounted)	// ensure the user has not left this component while we've been awaiting SearchPodcastIndex()
				setResponseData(results);
		}
		PreformSearch();

		return function cleanup () {
			mounted = false;
		};

	}, [setResponseData, responseData]);

	// visuals
	return (
		<View style={styles.container}>
			<ControlDeck/>
			<TextInput 
				style={styles.textInput} 
				onChangeText={(value => setSearchTerm(value.replace(' ', '%20')))} 
				placeholder={'Search'}/>

			<Text style={styles.textTypeOne}>
				{searchTerm}
			</Text>
			<ScrollView>
				{
					(typeof(responseData.feeds) == 'undefined')? <Text> Please Wait... </Text>	:responseData.feeds.map((item, index) => 
						<TouchableOpacity key={index} onPress={() => navigation.navigate('ViewFeed', {_feed: item}) }>
							<Image  
								source={{uri: `${((item.artwork == '')? podNoImage:item.artwork)}`}} 
								style={{width:  Dimensions.get('window').width, height:  Dimensions.get('window').width}}/>
						</TouchableOpacity>
					)

				}
			</ScrollView>
		</View>
	); // close return ()
}
