import * as React from 'react';
import * as WebBrowser from 'expo-web-browser';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as firebase from 'firebase';
import '@firebase/firestore';
import { Provider } from 'react-redux';
import store from './redux/store';
import LoginScreen from './screens/LoginScreen.js';
import PodcastSearch from './screens/PodcastSearch.js';
import ViewSubscriptions from './screens/ViewSubscriptions.js';
import MainScreen from './screens/MainScreen.js';
import NowPlayingScreen from './screens/NowPlayingScreen.js';
import PlayStackScreen from './screens/PlayStackScreen.js';
import ViewDownloads from './screens/ViewDownloads.js';
import {ViewFeed} from './screens/ViewFeed.js';
import SettingsScreen from './screens/SettingsScreen.js';

let app = null;
const Stack = createStackNavigator();

// Initialize Firebase
if (!firebase.apps.length) {
	app = firebase.initializeApp({
		// Config 
		apiKey: process.env.APIKEY,
		authDomain: process.env.AUTHDOMAIN,
		databaseURL: process.env.DATABASEURL,
		projectId: process.env.PROJECTID,
		storageBucket: process.env.STORAGEBUCKET,
		messagingSenderId: process.env.MESSAGINGSENDERID,
		appId: process.env.APPID
	});
}

WebBrowser.maybeCompleteAuthSession();

/*	App()
 *	William Doyle
 *	Febuary 2021
 *	
 *	Set up navigation for other screens
 * */
export default function App() {
	return (
		<Provider store={store}>
			<NavigationContainer>
				<Stack.Navigator>
					<Stack.Screen
						name="Login"
						component={LoginScreen}
						options={{ title: 'Berry Face Login' ,
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>
					<Stack.Screen 
						name="Main" 
						component={MainScreen} 
						options={{title: 'Berry Face',
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>
					<Stack.Screen
						name="NowPlaying"
						component={NowPlayingScreen}
						options={{ title: 'Episode Title Here' ,
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>
					<Stack.Screen
						name="PlayStackScreen"
						component={PlayStackScreen}
						options={{ title: 'Your Play Stack' ,
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>
					<Stack.Screen 
						name="ViewFeed" 
						component={ViewFeed} 
						options={{title: 'View Feed',
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>
					<Stack.Screen 
						name="Subscriptions" 
						component={ViewSubscriptions} 
						options={{title: 'Your Subscriptions',
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>
					<Stack.Screen 
						name="Search" 
						component={PodcastSearch} 
						options={{title: 'Search for podcasts',
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>
					<Stack.Screen 
						name="ViewDownloads" 
						component={ViewDownloads} 
						options={{title: 'Downloaded Podcasts',
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>
					<Stack.Screen 
						name="SettingsScreen" 
						component={SettingsScreen} 
						options={{title: 'Settings',
							headerStyle: {
								backgroundColor: '#B4f8c8'
							}}}
					/>

				</Stack.Navigator>
			</NavigationContainer>
		</Provider>
	);
}
