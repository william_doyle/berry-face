import * as firebase from 'firebase';
import '@firebase/firestore';

import {GetPodcastDataByFeedId} from './Podcast.js';

import ACTIONS from './redux/actions.js';
// access redux managed state 
import store from './redux/store.js';

// W O R L D     F A C I N G     F U N C T I O N S 
export async function GetUsersSubscriptions() {
	return await FirebaseGetUsersSubscriptions();
}

export async function GetUsersSubscriptionsRealTime() {
	return await FirebaseGetUsersSubscriptionsRealTime();
}

export function AddSubscription (id, title) {
	FirebaseAddSubscription(id, title);
}

/*	GetFullSub()
 *	Gets the full data of a podcast from only the podcastId using data stored in our database
 *	William Doyle
 *	Does not need to be exported but also operates at an abstraction level such that if we 
 *	change the backend database (prehaps to mongoDB) this function should not need to be modified.
 * */
async function GetFullSub (partSub) {
	let fullsub = await GetPodcastDataByFeedId(partSub.podcastId);
	store.dispatch(ACTIONS.addSubscription(fullsub));
}

/*	Firebase logic for adding a subscription	
 *	William Doyle
 * */
function FirebaseAddSubscription (id, title) {
	var database = firebase.database();
	let userId = firebase.auth().currentUser.uid;
	let postsRef = database.ref('userPodData/' + userId).child('subscriptions');
	let newPostRef = postsRef.push();

	newPostRef.set({
		podcastId: `${id}`,
		podcastTitle: `${title}`,
	}/*, error => {
		if (error)
			console.log('FAILED TO SUBSCRIBE');
	}*/);

}

/*	Firebase logic to get user subscriptions
 *	William Doyle
 * */
async function FirebaseGetUsersSubscriptions() {
	let datafound;
	let userId = firebase.auth().currentUser.uid;
	await firebase.database().ref('/userPodData/' + userId + '/subscriptions').once('value')
		.then(function (snapshot) {
			datafound = snapshot.val();
		});
	return  Object.values(datafound); 
}

/*	Firebase logic to get user subscriptions in real time
 *	William Doyle
 *	May 1st 2021
 * */
async function FirebaseGetUsersSubscriptionsRealTime() {
	firebase.database()
		.ref('/userPodData/' + firebase.auth().currentUser.uid + '/subscriptions')
		.on('value', snapshot => {
			store.dispatch(ACTIONS.setSubscriptions([]));		// Clear Subscriptions
			let usrsubs_partial = Object.values(snapshot.val());// Gather current subscriptions from db
			usrsubs_partial.map(sub => GetFullSub(sub));		// use small db data to find the rest of the data via the podcastindex.org
		});
}

